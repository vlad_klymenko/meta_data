import os
import time
from stat import *


if __name__ == "__maim__":
    types = [
        'mode',
        'ino',
        'dev',
        'nlink',
        'uid',
        'gid',
        'size',
        'atime',
        'mtime',
        'ctime'
    ]

    st = os.stat("example.mts")

    print("Base data from bytes:")
    for itx, byte_point in enumerate(st):
        print("%s %s" % (types[itx], byte_point))

    print("\nParsed data:")

    print("It's File") if not S_ISDIR(st[ST_MODE]) else print("It's Dir")
    print("file last access:\t", time.asctime(time.localtime(st[ST_ATIME])))
    print("file modified:\t\t", time.asctime(time.localtime(st[ST_MTIME])))
    print("file changed:\t\t", time.asctime(time.localtime(st[ST_CTIME])))
    print("file size:\t\t\t %s MB" % (st[ST_SIZE] / 1024 / 1024))
